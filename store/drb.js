export const state = () => ({
  user: null
})

export const mutations = {
  SET_USER(state, user) {
    state.user = user
  }
}

export const actions = {
  async getToken({ }, { code, baseURL }) {
    const res = await this.$axios.post(baseURL + '/api/drb/token', {
      code,
    })
    return res
  },
  async getUser({ }, { token }) {
    const res = await this.$axios.get('https://api.dribbble.com/v2/user', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return res
  },
  async getShots({ }, { token }) {
    // console.log(token)
    const res = await this.$axios.get('https://api.dribbble.com/v2/user/shots', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    // console.log(res);
    return res.data
  }
}