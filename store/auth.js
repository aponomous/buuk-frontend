const cmsUrl = 'https://cms.buuk.co/'
const prefix = `api/v2`
const prefixV3 = `api/v3`

const api = {
  revokeToken: `${cmsUrl}api/aam/v2/jwt/revoke`,
  token: `${cmsUrl}api/aam/v2/authenticate`,
  user: `${cmsUrl}${prefix}/auth/user`,
  userV3: `${cmsUrl}${prefixV3}/auth/user`,
  social: `${cmsUrl}${prefix}/auth/social`,
  password: `${cmsUrl}${prefix}/auth/password`,
  checkPasswordKey: `${cmsUrl}${prefix}/auth/password-key`
}

export const state = () => ({
  user: null
})

export const actions = {
  async getMe({ }) {
    const res = await this.$axios.$post(api.user)
    return res
  },
  async registerLoginSocial({ }, { token, channel = 'facebook' }) {
    const res = await this.$axios.$post(api.social, { token, channel })
    if (res.success) {
      this.$cookies.set('__session', {
        token: res.token
      }, {
        maxAge: 31556952,

      })
      return res
    }
  },
  async login({ }, { username, password }) {
    // load axios
    const res = await this.$axios.$post(api.token, {
      username,
      password,
      issueJWT: true
    }).catch(err => {
      const reason = err.response.data.reason
      let newMessage
    })
    if (!res) return {
      success: false,
      reason: 'to fix this'
    }
    if (res.jwt) {
      this.$cookies.set('__session', {
        token: res.jwt.token
      }, {
        maxAge: 31556952,

      })
      return {
        success: true,
      }
    }
  },
  async logout({ dispatch, commit }) {
    // const res = await this.$axios.$post(api.revokeToken)
    this.$cookies.remove('__session')
    return (window.location.href = '/')
  },
  async register(
    { commit },
    { email, password }
  ) {
    const res = await this.$axios.$post(`${api.userV3}/register`, {
      email,
      password,
    })
    return res
  }
}

export const mutations = {
  SET_USER(state, user) {
    state.user = user
  },
}