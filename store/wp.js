const cmsUrl = 'https://cms.buuk.co/'
const api = {
  cpt: `${cmsUrl}api/v3/cpt`,
  media: `${cmsUrl}api/v1/media`,
  project: `${cmsUrl}api/v1/project`,
  form: `${cmsUrl}api/v1/contact-submission`,
}

import qs from 'qs'

export const state = () => ({})

export const actions = {
  async listCPT({ }, _query) {
    const d = await this.$axios.$get(
      `${api.cpt}?${qs.stringify({
        ..._query,
        relatedContentPostNum: 0
      })}`
    )
    return d
  },
  async storeMedia({ }, { projectId, url, fileName, fileSize, fileType }) {
    const res = await this.$axios.$post(`${api.media}`, {
      projectId, url, fileName, fileSize, fileType
    })
    return res
  },
  async submitForm({ }, { email, name }) {
    const res = await this.$axios.$post(`${api.form}`, {
      email, name
    })
    return res
  }
}