import { storage } from '@/services/firebase'

export const state = () => ({
  uploadedTs: null
})

export const mutations = {
  SET_TS(state) {
    state.uploadedTs = new Date().getTime()
  }
}

export const actions = {
  async uploadFile({ dispatch, rootState, commit }, { projectId, fileArray }) {
    console.log(fileArray)
    const userId = rootState.auth.user._id
    const promises = fileArray.map(async (file) => {
      const { url, ...other } = await dispatch('asyncUpload', { userId, file })
      await dispatch('wp/storeMedia', { projectId, url, ...other }, {
        root: true
      })
      return url
    })
    await Promise.all(promises)
    return window.location.reload()
    // Upload to firebase
    // Sync to WordPress Media and User
  },
  async asyncUpload({ }, { userId, file }) {
    const fileName = new Date().getTime()
    const fileSize = file.size
    const fileType = file.type
    const storageObj = new storage
    const ref = storageObj.ref()
    const uploadTask = await ref.child(`user_${userId}/${fileName}`).put(file)
    const url = await uploadTask.ref.getDownloadURL()
    // console.log(url)
    return { url, fileName, fileSize, fileType }
  }
}