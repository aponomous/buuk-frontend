import qs from 'qs'
import _map from 'lodash.map'
import _clone from 'lodash.clone'

const cmsUrl = 'https://cms.buuk.co/'
const api = {
  projectCPT: `${cmsUrl}api/v3/cpt`,
  project: `${cmsUrl}api/v1/project`,
  projectUpdate: `${cmsUrl}api/v1/project-update`
}

export const state = () => ({
  title: null,
  coverPage: {},
  aboutPage: {},
  regularPages: [{ selected_layout: 0, images: [] }, { selected_layout: 0, images: [] }, { selected_layout: 0, images: [] }, { selected_layout: 0, images: [] }, { selected_layout: 0, images: [] }, { selected_layout: 0, images: [] }]
})

export const mutations = {
  SET_PROJECT(state, project) {
    state.title = project.title
    const pages = project.pages ? project.pages : state.regularPages
    state.coverPage = project.cover_page
    state.aboutPage = project.bio_page
    state.regularPages = _map(pages.map(x => {
      return {
        ...x,
        selected_layout: +x.selected_layout || 0
      }
    }), _clone)
  },
  SET_COVER(state, data) {
    state.coverPage = { ...state.coverPage, ...data }
  },
  SET_ABOUT(state, data) {
    state.aboutPage = { ...state.aboutPage, ...data }
  },
  SET_PAGE_LAYOUT_IMAGES(state, { index, imageArray }) {
    state.regularPages[index].images = imageArray
  },
  SET_PAGE_LAYOUT_OPTION(state, { index, selectedLayout }) {
    state.regularPages[index].selected_layout = selectedLayout
  },
  SET_TEXT(state, { pageIndex, imageIndex, text, subtitle = false }) {
    state.regularPages[pageIndex].images[imageIndex][subtitle ? 'subtitle' : 'title'] = text
  },
}

export const actions = {
  async create({ }, { title }) {
    const res = await this.$axios.$post(api.project, {
      title
    })
    return res
  },
  async list({ }, query) {
    const res = await this.$axios.$get(`${api.projectCPT}?${qs.stringify({ ...query, type: 'project' })}`)
    return res
  },
  async delete({ }, { slug }) {
    const res = await this.$axios.$put(api.project, { slug })
    return res
  },
  async update({ state }, { slug }) {
    // console.log('need some throttling')
    // return true
    const res = await this.$axios.$put(api.projectUpdate, { progress: false, slug, cover_page: state.coverPage, pages: state.regularPages, bio_page: state.aboutPage })
    return res
  }
}