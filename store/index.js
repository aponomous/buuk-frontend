import templates from '@/config/template'
import { encode } from 'js-base64'

const layoutOptions = Object.entries(templates).map((x) => {
  return {
    title: x[0],
    ...x[1],
  }
})

export const state = () => ({
  isImgLibShowing: true,
  layoutOptions,
})

export const actions = {
  async nuxtServerInit({ dispatch, commit }, { app, redirect, req }) {
    let promises = []
    const session = app.$cookies.get('__session')
    if (session && session.token) {
      promises.push(dispatch('auth/getMe'))
    } else return
    try {
      const [user = null] = await Promise.all(promises)
      if (user) {
        commit('auth/SET_USER', user)
        if (req.originalUrl.indexOf('/project') === -1) return redirect('/projects')
      }
    } catch (err) {
      console.error('Error on [nuxtServerInit] action.', err)
      return redirect('/')
    }
    return
  },
  nuxtClientInit() { },
  async export({ rootState }, { baseURL, slug }) {
    const res = await this.$axios.post(baseURL + '/api/export', {
      slug,
      baseURL
    }, {
      responseType: 'arraybuffer'
    })
    return res
  }
}

export const mutations = {
  SET_IMAGE_LIB_SHOWING(state, bool) {
    state.isImgLibShowing = bool
  },
  SET_COVER_IMAGE(state, { imageURL }) {
    state.pages[0].image.url = imageURL
  },
  SET_COVER_NAME(state, name) {
    state.pages[0].name = name
  },
  SET_COVER_TITLE(state, title) {
    state.pages[0].title = title
  },
  SET_PAGE_ORDER(state, arr) {
    state.pages.arr = arr
  }
}