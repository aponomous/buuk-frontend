const firebaseVersion = '7.16.1'

module.exports = {
  debug: true,
  telemetry: false,
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Buuk – Drag. Drop. Share.',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }, {
        rel: 'stylesheet',
        href:
          'https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css',
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;700&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css',
      },
    ],
    script: [{
      src: `https://www.gstatic.com/firebasejs/${firebaseVersion}/firebase-app.js`,
      body: true
    },
    {
      src: `https://www.gstatic.com/firebasejs/${firebaseVersion}/firebase-storage.js`,
      body: true
    }, {
      src: 'https://unpkg.com/panzoom@9.2.4/dist/panzoom.min.js',
      body: true
    }, {
      src: 'https://unpkg.com/interactjs/dist/interact.min.js',
      body: true
    }, {
      src:
        'https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js',
      body: true,
    }, {
      src: '/scripts/FitText.js',
      body: true
    }]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    {
      src: '@/plugins/axios.js'
    },
    {
      src: '@/plugins/vee-validate.js',
      mode: 'client'
    },
    {
      src: '@/plugins/buefy.js'
    },
    {
      src: '@/plugins/injections.js'
    },
    {
      src: '@/plugins/both.js'
    },
    {
      src: '@/plugins/client.js',
      mode: 'client'
    },
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  // serverMiddleware: [
  //   redirectSSL.create({
  //     enabled: process.env.NODE_ENV === 'production'
  //   }),
  // ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/localforage',
    'nuxt-client-init-module',
    'cookie-universal-nuxt',
    '@nuxtjs/dotenv',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/gtm'
    // Doc: https://github.com/nuxt/content
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  gtm: {
    id: 'GTM-5CD56N5'
  },
  axios: {},
  localforage: {
    /* module options */
    name: 'buukco_storage'
  },
  /*
  ** Content module configuration
  ** See https://content.nuxtjs.org/configuration
  */
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    babel: {
      babelrc: true
    },
    extractCSS: true,
    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    },
  }
}
