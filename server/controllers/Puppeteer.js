const puppeteer = require('puppeteer')
const _map = require('async/map')

module.exports = async (prefixURL, pageArray, _format = 'A4') => {
  // console.log(urls)
  const format = _format
  const browser = await puppeteer.launch({ headless: true })
  const bufferList = await _map(pageArray, async (_p) => {
    const page = await browser.newPage()
    const url = `${prefixURL}&${_p}`
    console.log(`Capturing: ${url}`)
    await page.goto(decodeURIComponent(url), { waitUntil: 'networkidle2' })
    const buffer = await page.pdf({
      scale: 1,
      format,
      landscape: true,
      printBackground: true,
      margin: {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
      },
      displayHeaderFooter: false,
    })
    return buffer
  })
  await browser.close()
  return bufferList
}