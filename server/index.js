// @ts-nocheck
const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()
require('dotenv').config()
const serverEnv = process.env.NODE_ENV === 'development' ? 'TEST' : 'PROD'
app.set('DRB_ENV', {
  redirect_uri: process.env[`${serverEnv}_DRB_CALLBACK`],
  client_id: process.env[`${serverEnv}_DRB_ID`],
  client_secret: process.env[`${serverEnv}_DRB_SECRET`],
})

const ExportRouter = require('./routes/ExportRouter')
const DrbRouter = require('./routes/DrbRouter')

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(compression())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
  // Give nuxt middleware to express
  app.use((req, res, next) => {
    res.append('Cache-Control', 'public, no-store')
    // https://cheatsheetseries.owasp.org/cheatsheets/Clickjacking_Defense_Cheat_Sheet.html
    res.append('X-Frame-Options', 'DENY')
    res.append('Content-Security-Policy', "frame-ancestors 'none'")
    res.append('X-XSS-Protection', '1; mode=block')
    // https://cheatsheetseries.owasp.org/cheatsheets/Clickjacking_Defense_Cheat_Sheet.html?q=Strict-Transport-Security
    res.append('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
    res.append('X-Content-Type-Options', 'nosniff')
    next()
  })
  app.use(express.json())
  app.use(
    morgan('tiny', {
      skip(req, res) {
        return res.statusCode < 400
      }
    })
  )
  app.use('/api/drb', DrbRouter)
  app.use('/api/export', ExportRouter)
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
