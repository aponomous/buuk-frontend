const { Router } = require('express')
const { PDFDocument } = require('pdf-lib')
const Capture = require('../controllers/Puppeteer')
const router = Router()

router.post('/', async (req, res) => {
  const projectSlug = req.body.slug
  const baseURL = req.body.baseURL
  const bufferList = await Capture(`${baseURL}/projects/${projectSlug}/export?code=ko0ji9hu8`,
    [
      'coverPage=1',
      'aboutPage=1',
      'regularIndex=1',
      'regularIndex=2',
      'regularIndex=3',
      'regularIndex=4',
      'regularIndex=5',
      'regularIndex=6'
    ]
  )

  const doc = await PDFDocument.create()
  for (const buffer of bufferList) {
    const pdfDoc = await PDFDocument.load(toArrayBuffer(buffer))
    const [page] = await doc.copyPages(pdfDoc, [0])
    doc.addPage(page)
  }
  const pdfBytes = await doc.save()
  const buffer = Buffer.from(pdfBytes)
  const filename = `buuk-${new Date().getTime()}.pdf`
  res.writeHead(200, {
    'Content-Type': 'application/pdf',
    'Content-Disposition': `attachment; filename=${filename}`,
    'Content-Length': buffer.length,
  })
  res.end(buffer)
})

module.exports = router

function toArrayBuffer(buffer) {
  var ab = new ArrayBuffer(buffer.length);
  var view = new Uint8Array(ab);
  for (var i = 0; i < buffer.length; ++i) {
    view[i] = buffer[i];
  }
  return ab;
}