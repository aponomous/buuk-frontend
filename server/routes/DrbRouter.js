const { Router } = require('express')
const router = Router()
const axios = require('axios')

router.post('/token', async (req, res) => {
  const code = req.body.code
  console.log(req.app.get('DRB_ENV'))
  const tokenRes = await axios.post(`https://dribbble.com/oauth/token`, {
    code,
    ...req.app.get('DRB_ENV')
  })
  // console.log(tokenRes)
  return res.json(tokenRes.data)
})

module.exports = router