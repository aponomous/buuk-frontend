FROM node:14

# Installs latest Chromium (77) package.
RUN apt-get update \
  && apt-get install -y wget gnupg \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y libxss1 google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
# ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

# Add user so we don't need --no-sandbox.
RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
  && mkdir -p /home/pptruser/Downloads /app /usr/src/app \
  && chown -R pptruser:pptruser /home/pptruser \
  && chown -R pptruser:pptruser /app \
  && chown -R pptruser:pptruser /usr/src/app

WORKDIR /usr/src/app

# Run everything after as non-privileged user.
USER pptruser

# Install app dependencies
# Bundle app source
ADD . /usr/src/app
RUN yarn install
RUN yarn b

# Set environment variables
ENV NODE_ENV production
ENV HOST 0.0.0.0
ENV PORT 8080

# Clear the cache
RUN yarn cache clean

EXPOSE 8080
CMD [ "yarn", "start" ]