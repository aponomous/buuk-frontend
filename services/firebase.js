// @ts-nocheck
const config = require('@/config/firebase')

async function initFirebase() {
  // Initialize Firebase
  if (firebase && !firebase.apps.length) {
    await firebase.initializeApp(config)
    return
  }
  return
}

function storage() {
  return firebase.storage()
}


export {
  initFirebase,
  storage
}