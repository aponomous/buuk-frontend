import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import VueScrollTo from 'vue-scrollto'


Vue.use(VueScrollTo)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/error.png',
  loading: '/images/loading.svg',
  attempt: 1,
  lazyComponent: true
})