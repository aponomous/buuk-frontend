const axios = ({ $axios, redirect, isDev, app }) => {
  //
  $axios.onRequest((config) => {
    const session = app.$cookies.get('__session')
    if (session && session.token)
      config.headers['Authentication'] = `Bearer ${session.token}`
    if (config?.url?.indexOf('noPageLoading') < 0) {
      // store.commit('SET_PAGE_LOADING', true)
    }
    isDev && console.log(`============================`)
    isDev &&
      console.log(
        `== ${config.method.toUpperCase()}: ${config.url}`
      )
    isDev &&
      console.log(
        `== token length: ${config.headers['Authentication']?.length}`
      )
    isDev && console.log(`============================`)
    return config
  })
  $axios.onResponse((response) => {
    // store.commit('SET_PAGE_LOADING', false)
  })
  $axios.onRequestError((error) => {
    app.$cookies.remove('__session')
    return redirect('/')
  })
  $axios.onResponseError((error) => {
    app.$cookies.remove('__session')
    return redirect('/')
  })
  $axios.onError((error) => {
    app.$cookies.remove('__session')
    return redirect('/')
  })
}

export default axios