import Vue from 'vue'
import {
  ValidationProvider,
  ValidationObserver,
  extend,
  setInteractionMode
} from 'vee-validate'
import { required, email, regex, confirmed, is } from 'vee-validate/dist/rules'

setInteractionMode('eager')
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

extend('password', {
  params: ['target'],
  validate(value, { target }) {
    return value === target
  },
  message: 'Password confirmation does not match'
})
extend('email', email)
extend('regex', regex)
extend('confirmed', confirmed)
extend('is', is)
extend('required', {
  ...required,
  message: 'Please specify'
})