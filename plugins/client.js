import * as FB from '@/services/firebase'

export default async () => {
  if (process.client) {
    await FB.initFirebase()
  }
}
