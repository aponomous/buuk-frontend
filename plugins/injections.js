const injections = ({ app, store }, inject) => {
  inject('registerLoginSocial', async ({
    token,
    channel = 'facebook',
    redirect = '/'
  }) => {
    const res = await store.dispatch('auth/registerLoginSocial', {
      channel,
      token
    })
    if (res.success) {
      return (window.location.href = redirect)
    }
  })
  inject('changeView', (view, pageIndex = 0) => {
    return app.router.push({
      ...app.router.currentRoute,
      query: {
        ...app.router.currentRoute.query,
        view,
        pageIndex
      },
    })
  })
}

export default injections